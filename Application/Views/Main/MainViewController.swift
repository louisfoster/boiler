//
//  MainViewController.swift
//  boiler
//
//  Created by Louis Foster on 29/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

/*
 
 The purpose of this class is to do the initial view
 setup and ensure an instance of metal is available
 to the app. The valid metal device is then used to
 create the renderer, which takes it from there.
 
 This view current handles key input events for macOS.
 This might change as classes are abstracted to handle
 more gestures and complex views.
 
 */

#if os( iOS )
    import UIKit
    typealias PlatformViewController = UIViewController
#else
    import Cocoa
    typealias PlatformViewController = NSViewController
#endif

import MetalKit

enum MetalViewError: Error
{
    case noView
    case noDevice
}

protocol MetalViewControllerProtocol: class
{
    var renderer: RendererProtocol? { get }
    
    func setupMTK( ) throws
    func setupWithPrintErrors( )
    
    #if os( OSX )
        var landscapeMode: Bool? { get }
    
        func resizeWindow( sender: AnyObject? )
        func handleKeyDown( with event: NSEvent ) -> Bool
    #endif
}

class MainViewController: PlatformViewController, MetalViewControllerProtocol
{
    // MARK: Properties
    
    private(set) var renderer: RendererProtocol?
    
    #if os( OSX )
        private(set) var landscapeMode: Bool?
    #endif
    
    // MARK: Initialization
    
    func setupMTK( ) throws
    {
        guard let _mtkView = self.view as? MTKView
        else
        {
            throw MetalViewError.noView
        }
        
        _mtkView.device = MTLCreateSystemDefaultDevice( )
        
        guard let _device = _mtkView.device
        else
        {
            throw MetalViewError.noDevice
        }
        
        self.renderer = try Renderer( _device, _mtkView )
        
        _mtkView.delegate = self.renderer
        _mtkView.preferredFramesPerSecond = 60
        _mtkView.clearColor = MTLClearColor( red: 0.0,
                                             green: 0.0,
                                             blue: 0.0,
                                             alpha: 1.0 )
    }
    
    func setupWithPrintErrors( )
    {
        do
        {
            try self.setupMTK( )
        }
        catch MetalViewError.noView
        {
            print( "No view found during setup." )
        }
        catch MetalViewError.noDevice
        {
            print( "No device found during setup." )
        }
        catch RendererError.noPipelineState
        {
            print( "No pipeline state found during setup." )
        }
        catch RendererError.noCommandQ
        {
            print( "No command queue found during setup." )
        }
        catch
        {
            print( "Unknown setup error." )
        }
    }
    
    // MARK: - View Controller Life Cycle
    
    override func viewDidLoad( )
    {
        super.viewDidLoad( )
        
        self.setupWithPrintErrors( )
        
        #if os( OSX )
            self.landscapeMode = false
        
            NSEvent.addLocalMonitorForEvents( matching: .flagsChanged )
            {
                return $0
            }
        
            NSEvent.addLocalMonitorForEvents( matching: .keyDown )
            {
                if self.handleKeyDown(with: $0)
                {
                    return nil
                }
                else
                {
                    return $0
                }
            }
        #endif
    }
    
    // MARK: - macOS window methods
    
    #if os( OSX )
        @IBAction
        func resizeWindow( sender: AnyObject? )
        {
            var mode = true
            var width: Double = 568.0
            var height: Double = 320.0
            
            if let _mode = self.landscapeMode
            {
                mode = !_mode
            }
            
            if !mode // make portrait
            {
                width = 320.0
                height = 568.0
            }
            
            let currentPosition = self.view.window?.frame.origin
            let rect = NSRect( x: Double( currentPosition?.x ?? 0 ),
                               y: Double( currentPosition?.y ?? 0 ),
                               width: width,
                               height: height )
            self.view.window?.setFrame( rect, display: true )
            
            self.landscapeMode = mode
        }
    
        // Allow view to receive keypress (remove the purr sound)
        override var acceptsFirstResponder : Bool
        {
            return true
        }
    
        func handleKeyDown( with event: NSEvent ) -> Bool
        {
            switch event.modifierFlags.intersection( .deviceIndependentFlagsMask )
            {
            case [ .command ] where event.characters == "r":
                self.resizeWindow( sender: nil )
                return true
            default:
                break
            }
            
            return false
        }
    #endif
}
