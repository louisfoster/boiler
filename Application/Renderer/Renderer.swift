//
//  Renderer.swift
//  boiler
//
//  Created by Louis Foster on 29/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

/*
 
 The renderer is called by the view controller to instantiate
 the visual world itself after the view itself has been
 created and the Metal instance has been established
 
 */

import MetalKit
import simd

enum RendererError: Error
{
    case noPipelineState
    case noCommandQ
}

protocol RendererProtocol: class, MTKViewDelegate
{
    var device: MTLDevice { get }
    var commandQ: MTLCommandQueue { get }
    var lastFrameTimestamp: CFTimeInterval { get }
    var camera: CameraProtocol? { get }
    
    // TODO: ?
    var pipelineState: MTLRenderPipelineState { get }
    
    // TODO: Manage with entity system?
    var objectToDraw: Node? { get }
    
    // MARK: Rendering
    func updateLogic( timeSinceLastUpdate: CFTimeInterval )
    func renderObjects( drawable: CAMetalDrawable )
}

extension RendererProtocol
{
    // MARK: Helpers
    
    func getViewDimensions( _ mtkView: MTKView ) -> ViewDimensions2D
    {
        return ViewDimensions2D( width: Float( mtkView.bounds.size.width ),
                                 height: Float( mtkView.bounds.size.height ) )
    }
    
    func render( _ drawable: CAMetalDrawable? )
    {
        // Prevent drawing if drawable not available
        
        guard let _drawable = drawable
            else
        {
            return
        }
        
        self.renderObjects( drawable: _drawable )
    }
}

class Renderer: NSObject, RendererProtocol
{
    
    // MARK: Properties
    
    private(set) var device: MTLDevice
    
    private(set) var commandQ: MTLCommandQueue
    
    private(set) var lastFrameTimestamp: CFTimeInterval
    
    private(set) var pipelineState: MTLRenderPipelineState
    
    private(set) var projectionMatrix: float4x4?
    
    private(set) var worldModelMatrix: float4x4?
    
    private(set) var objectToDraw: Node?
    
    private(set) var camera: CameraProtocol?

    // MARK: Initialization
    
    init( _ _device: MTLDevice, _ _mtkView: MTKView ) throws
    {
        self.device = _device
        
        let defaultLibrary = _device.makeDefaultLibrary( )
        let vertexProgram = defaultLibrary?.makeFunction( name: "basic_vertex" )
        let fragmentProgram = defaultLibrary?.makeFunction( name: "basic_fragment" )
        
        let pipelineStateDescriptor = MTLRenderPipelineDescriptor( )
        pipelineStateDescriptor.vertexFunction = vertexProgram
        pipelineStateDescriptor.fragmentFunction = fragmentProgram
        pipelineStateDescriptor.colorAttachments[ 0 ].pixelFormat = .bgra8Unorm
        
        guard let _pipelineState = try? _device.makeRenderPipelineState( descriptor: pipelineStateDescriptor )
        else
        {
            throw RendererError.noPipelineState
        }
        
        self.pipelineState = _pipelineState
        
        guard let _commandQ = _device.makeCommandQueue( )
        else
        {
            throw RendererError.noCommandQ
        }
        
        self.commandQ = _commandQ
        
        self.lastFrameTimestamp = 0.0
        
        self.objectToDraw = Triangle( device: _device )
        
        super.init( )
        
        let dimensions: ViewDimensions2D = self.getViewDimensions( _mtkView )
        self.camera = Camera( _device, dimensions, x: 0, y: 0, z: 4.0 )
    }
    
    // MARK: Rendering Methods
    
    func updateLogic( timeSinceLastUpdate: CFTimeInterval )
    {
        self.objectToDraw?.updateWithDelta( delta: timeSinceLastUpdate )
    }
    
    func renderObjects( drawable: CAMetalDrawable )
    {
        if let _projection = self.camera?.projectionMatrix,
           let _view = self.camera?.getViewMatrix()
        {
            self.objectToDraw?.render( commandQ: self.commandQ,
                                       pipelineState: self.pipelineState,
                                       drawable: drawable,
                                       viewMatrix: _view,
                                       projectionMatrix: _projection,
                                       clearColor: nil )
        }
    }
}

extension Renderer
{
    // MARK: MTKViewDelegate Methods
    
    func draw( in view: MTKView )
    {
        self.render( view.currentDrawable )
        
        if self.lastFrameTimestamp == 0.0
        {
            self.lastFrameTimestamp = CACurrentMediaTime()
        }

        let elapsed = CACurrentMediaTime() - self.lastFrameTimestamp
        self.lastFrameTimestamp = CACurrentMediaTime()

        self.updateLogic( timeSinceLastUpdate: elapsed )
    }
    
    func mtkView( _ mtkView: MTKView, drawableSizeWillChange size: CGSize )
    {
        let dimensions = self.getViewDimensions( mtkView )
        self.camera?.updateProjectionMatrix( dimensions )
    }
}
