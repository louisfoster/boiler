//
//  ViewDimensions2D.swift
//  boiler
//
//  Created by Louis Foster on 29/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation

struct ViewDimensions2D
{
    var width: Float
    var height: Float
}
