//
//  Vertex.swift
//  boiler
//
//  Created by Louis Foster on 29/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Foundation

struct VertexPositionAndColor
{
    var x, y, z: Float // Position data
    var r, g, b, a: Float // Color data
    
    func floatBuffer( ) -> [ Float ]
    {
        return [ x, y, z, r, g, b, a ]
    }
}
