//
//  BufferProvider.swift
//  rayw-p3
//
//  Created by Louis Foster on 25/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Metal
import simd

class BufferProvider: NSObject
{
    var availableResourcesSemaphore: DispatchSemaphore
    let inflightBuffersCount: Int
    private var uniformsBuffers: [MTLBuffer]
    private var availableBufferIndex: Int = 0
    private var dataSize: Int = MemoryLayout<Float>.size * float4x4.numberOfElements( )
    
    init( device: MTLDevice,
          inflightBuffersCount _inflightBuffersCount: Int,
          sizeOfUniformsBuffer: Int )
    {
        self.inflightBuffersCount = _inflightBuffersCount
        self.uniformsBuffers = [ MTLBuffer ]( )
        self.availableResourcesSemaphore = DispatchSemaphore( value: self.inflightBuffersCount )
        
        for _ in 0...self.inflightBuffersCount - 1
        {
            let uniformsBuffer = device.makeBuffer(length: sizeOfUniformsBuffer, options: [])!
            self.uniformsBuffers.append( uniformsBuffer )
        }
    }
    
    deinit
    {
        for _ in 0...self.inflightBuffersCount
        {
            self.availableResourcesSemaphore.signal( )
        }
    }
    
    func nextUniformsBuffer( projectionMatrix _projectionMatrix: float4x4,
                             modelViewMatrix _modelViewMatrix: float4x4 ) -> MTLBuffer
    {
        var projectionMatrix = _projectionMatrix
        var modelViewMatrix = _modelViewMatrix
        
        let buffer = self.uniformsBuffers[ self.availableBufferIndex ]
        let bufferPointer = buffer.contents( )
        memcpy( bufferPointer, &modelViewMatrix, self.dataSize )
        memcpy( bufferPointer.advanced( by: self.dataSize ), &projectionMatrix, self.dataSize )
        
        self.availableBufferIndex += 1
        if self.availableBufferIndex == self.inflightBuffersCount
        {
            self.availableBufferIndex = 0
        }
        
        return buffer
    }
}
