//
//  AppDelegate.swift
//  boiler-mac
//
//  Created by Louis Foster on 29/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate
{
    func applicationDidFinishLaunching( _ aNotification: Notification ) { }

    func applicationWillTerminate( _ aNotification: Notification ) { }
}
