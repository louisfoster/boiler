//
//  BasicVertexShader.metal
//  boiler
//
//  Created by Louis Foster on 30/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;
#include "../Common/BasicCommon.h"

struct VertexIn
{
    packed_float3 position;
    packed_float4 color;
};

struct Uniforms
{
    float4x4 modelMatrix;
    float4x4 projectionMatrix;
};

vertex VertexOut basic_vertex(
                              const device VertexIn *vertex_array [ [ buffer( 0 ) ] ],
                              const device Uniforms &uniforms [ [ buffer( 1 ) ] ],
                              unsigned int vid [ [ vertex_id ] ]
                              )
{
    float4x4 proj_Matrix = uniforms.projectionMatrix;
    float4x4 mv_Matrix = uniforms.modelMatrix;
    
    VertexIn VertexIn = vertex_array[ vid ];
    
    VertexOut VertexOut;
    VertexOut.position = proj_Matrix * mv_Matrix * float4( VertexIn.position, 1 );
    VertexOut.color = VertexIn.color;
    
    return VertexOut;
}
