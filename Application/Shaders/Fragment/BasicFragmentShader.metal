//
//  BasicFragmentShader.metal
//  boiler
//
//  Created by Louis Foster on 30/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;
#include "../Common/BasicCommon.h"

fragment half4 basic_fragment( VertexOut interpolated [ [ stage_in ] ] )
{
    return half4( interpolated.color[ 0 ],
                  interpolated.color[ 1 ],
                  interpolated.color[ 2 ],
                  interpolated.color[ 3 ] );
}
