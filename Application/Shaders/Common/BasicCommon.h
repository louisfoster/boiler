//
//  BasicCommon.h
//  boiler
//
//  Created by Louis Foster on 30/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

#ifndef BasicCommon_h
#define BasicCommon_h

struct VertexOut
{
    float4 position [ [ position ] ];
    float4 color;
};

#endif /* BasicCommon_h */
