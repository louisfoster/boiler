//
//  Camera.swift
//  boiler
//
//  Created by Louis Foster on 30/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import MetalKit
import simd

protocol CameraProtocol: class
{
    /*
     camera has position, and therefore an inverse world matrix
     when the drawable area size changes, it's proj matrix updates
     
     a node could have a camera, and a specific camera node can
     then access the node's rotation, position, model view matrix
     and all the camera needs is the ability to invert the mvm
     and be provided with updates to the camera with size change
 
    */
    var fieldOfView: Float? { get set }
    var nearPlane: Float? { get set }
    var farPlane: Float? { get set }
    var projectionMatrix: float4x4? { get }
    
    // Invert model matrix from parent node
    func getViewMatrix( ) -> float4x4
    // update projection matrix with new dimensions
    func updateProjectionMatrix( _ dimensions: ViewDimensions2D )
}

class Camera: Node, CameraProtocol
{
    var fieldOfView: Float?
    
    var nearPlane: Float?
    
    var farPlane: Float?
    
    private(set) var projectionMatrix: float4x4?
    
    init( _ device: MTLDevice,
          _ dimensions: ViewDimensions2D,
          x: Float = 0,
          y: Float = 0,
          z: Float = 0 )
    {
        super.init( name: "Camera", device: device )
        
        self.positionX = x
        self.positionY = y
        self.positionZ = z
        
        self.updateProjectionMatrix( dimensions )
    }
    
    func getViewMatrix( ) -> float4x4
    {
        return self.modelMatrix( ).inverse
    }
    
    func updateProjectionMatrix( _ dimensions: ViewDimensions2D )
    {
        self.projectionMatrix = self.buildProjectionMatrix( dimensions )
    }
    
    private func buildProjectionMatrix( _ dimensions: ViewDimensions2D,
                                        fieldOfView fov: Float = 85.0,
                                        nearPlane near: Float = 0.01,
                                        farPlane far: Float = 100.0 ) -> float4x4
    {
        let aspectRatio = dimensions.width / dimensions.height
        return float4x4.makePerspectiveViewAngle( float4x4.degrees( toRad: fov ),
                                                  aspectRatio: aspectRatio,
                                                  nearZ: near,
                                                  farZ: far )
    }
}
