//
//  Node.swift
//  boiler
//
//  Created by Louis Foster on 29/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

/*
 
 This is the base class to give objects life
 
 */

import Metal
import QuartzCore
import simd

class Node
{
    let device: MTLDevice
    let name: String
    var vertexCount: Int?
    var vertexBuffer: MTLBuffer?
    var bufferProvider: BufferProvider?
    
    var positionX: Float = 0.0
    var positionY: Float = 0.0
    var positionZ: Float = 0.0
    var rotationX: Float = 0.0
    var rotationY: Float = 0.0
    var rotationZ: Float = 0.0
    var scale: Float = 1.0
    
    var time: CFTimeInterval = 0.0
    
    init( name _name: String,
          device _device: MTLDevice,
          vertices _vertices: Array<VertexPositionAndColor>? = nil )
    {
        self.name = _name
        self.device = _device
        
        if let v = _vertices
        {
            var vertexData = Array<Float>( )
            
            for vertex in v
            {
                vertexData += vertex.floatBuffer( )
            }
            
            let dataSize = vertexData.count * MemoryLayout.size( ofValue: vertexData[ 0 ] )
            self.vertexBuffer = _device.makeBuffer( bytes: vertexData,
                                                    length: dataSize,
                                                    options: [ ] )!
            
            self.vertexCount = v.count
            let bufferSize = MemoryLayout<Float>.size * float4x4.numberOfElements( ) * 2
            self.bufferProvider = BufferProvider( device: self.device,
                                                  inflightBuffersCount: 3,
                                                  sizeOfUniformsBuffer: bufferSize )
        }
    }
    
    func render( commandQ: MTLCommandQueue,
                 pipelineState: MTLRenderPipelineState,
                 drawable: CAMetalDrawable,
                 viewMatrix: float4x4,
                 projectionMatrix: float4x4,
                 clearColor: MTLClearColor? )
    {
        guard let vc = self.vertexCount, let bp = self.bufferProvider
        else
        {
            return
        }
        
        _ = bp.availableResourcesSemaphore
              .wait( timeout: DispatchTime.distantFuture )
        
        let renderPassDescriptor = MTLRenderPassDescriptor( )
        let attachment = renderPassDescriptor.colorAttachments[ 0 ]
        attachment?.texture = drawable.texture
        attachment?.loadAction = .clear
        attachment?.clearColor = MTLClearColor( red: 0.0,
                                                green: 0.0,
                                                blue: 0.0,
                                                alpha: 1.0 )
        attachment?.storeAction = .store
        
        let commandBuffer = commandQ.makeCommandBuffer( )!
        commandBuffer.addCompletedHandler { (_) in
            bp.availableResourcesSemaphore.signal( )
        }
        
        let renderEncoder =
            commandBuffer.makeRenderCommandEncoder( descriptor: renderPassDescriptor )!
        renderEncoder.setCullMode( MTLCullMode.front )
        renderEncoder.setRenderPipelineState( pipelineState )
        renderEncoder.setVertexBuffer( self.vertexBuffer, offset: 0, index: 0 )
        
        var nodeModelMatrix = self.modelMatrix( )
        nodeModelMatrix.multiplyLeft( viewMatrix )
        let uniformBuffer =
            bp.nextUniformsBuffer( projectionMatrix: projectionMatrix,
                                   modelViewMatrix: nodeModelMatrix )
        renderEncoder.setVertexBuffer( uniformBuffer, offset: 0, index: 1 )
        renderEncoder.setFragmentBuffer( uniformBuffer, offset: 0, index: 1 )
        
        renderEncoder.drawPrimitives( type: .triangle,
                                      vertexStart: 0,
                                      vertexCount: vc,
                                      instanceCount: vc / 3 )
        renderEncoder.endEncoding( )
        
        commandBuffer.present( drawable )
        commandBuffer.commit( )
    }
    
    func modelMatrix( ) -> float4x4
    {
        var matrix = float4x4( )
        matrix.translate( x: self.positionX, y: self.positionY, z: self.positionZ )
        matrix.rotateAroundX( x: self.rotationX, y: self.rotationY, z: self.rotationZ )
        matrix.scale( self.scale )
        return matrix
    }
    
    func updateWithDelta( delta: CFTimeInterval )
    {
        self.time += delta
    }
}
