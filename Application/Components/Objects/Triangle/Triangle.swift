//
//  Triangle.swift
//  boiler
//
//  Created by Louis Foster on 29/8/18.
//  Copyright © 2018 Louis Foster. All rights reserved.
//

import MetalKit

class Triangle: Node
{
    init( device _device: MTLDevice )
    {
        let V0 = VertexPositionAndColor( x: 0.0, y: 1.0, z: 0.0, r: 1.0, g: 0.0, b: 0.0, a: 1.0 )
        let V1 = VertexPositionAndColor( x: -1.0, y: -1.0, z: 0.0, r: 0.0, g: 1.0, b: 0.0, a: 1.0 )
        let V2 = VertexPositionAndColor( x: 1.0, y: -1.0, z: 0.0, r: 0.0, g: 0.0, b: 1.0, a: 1.0 )
        
        let verticesArray = [ V0, V1, V2 ]
        super.init( name: "Triangle", device: _device, vertices: verticesArray )
    }
    
    override func updateWithDelta(delta: CFTimeInterval)
    {
        super.updateWithDelta( delta: delta )
        
        let secsPerMove: Float = 6.0
        self.rotationX = sinf( Float( self.time ) * 2.0 * Float.pi / secsPerMove )
    }
}
